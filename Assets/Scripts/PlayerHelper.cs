﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// всё работает
public class PlayerHelper : MonoBehaviour
{
    public float MaxSpeed { get; set; }
    [HideInInspector]
    public float RotationSpeed { get; set; } // в итоге не смог применить как-то скорость поворота
    public float MovementSpeed { get; set; }
    public float MaxRotSpeed { get; set; }
    public float MinSpeed { get; set; }

    private Camera cam;

    void Start()
    {
        cam = GetComponentInChildren<Camera>();

        RotationSpeed = 75.0f;
        MaxSpeed = 0.2f;
        MinSpeed = 0.05f;

        MovementSpeed = 0.05f;
        MaxRotSpeed = 100.0f;
    }
    
    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");

        // ускорение
        MovementSpeed = h == 0 ? MovementSpeed - Time.fixedDeltaTime * 0.015f : MovementSpeed + Time.fixedDeltaTime * 0.02f;

        // скорость поворота
        RotationSpeed = MovementSpeed / MaxSpeed * MaxRotSpeed;
        RotationSpeed = RotationSpeed < 75.0f ? 75.0f : RotationSpeed;

        // проверка на выход за лимиты
        if (MovementSpeed <= MinSpeed) MovementSpeed = MinSpeed;
        if (MovementSpeed >= MaxSpeed) MovementSpeed = MaxSpeed;

        // поворот
        if (h == 1)
        {
            transform.Rotate(new Vector3(0, 2, 0));
        }
        else
        if (h == -1)
        {
            transform.Rotate(new Vector3(0, -2, 0));
        }

        // передвижение
        transform.position += cam.transform.forward * MovementSpeed;

        if (transform.position.y < -3.0f)
        {
            SceneManager.LoadScene(0);
            //Time.timeScale = 0;
            //Destroy(gameObject);
        }
    }

}
