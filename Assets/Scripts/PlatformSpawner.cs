﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// всё работает
public class PlatformSpawner : MonoBehaviour
{

    public GameObject Platform;
    public GameObject LeftPoint;
    public GameObject RightPoint;
    public GameObject InistiateWall;

    // вектор X отвечает за левый или правый спавн платформы
    // вектор Y - константная высота
    // вектор Z - основной, вдоль него двигается игрок

    // дистанция между платформами
    private const float MaxDistance = 7;
    private float Distance { get; set; }
    private const float incDistance = 0.25f;

    private GameObject player;

    // в ней хранится Vector3 последней созданной платформы
    private Vector3 LastPoint;
    // в ней хранится Vector3 следующей платформы
    private Vector3 NextPoint;

    private float X { get; set; }
    private float Y { get; set; }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Distance = 5.3f;
        NextPoint = LeftPoint.transform.position;
        X = transform.position.x;
        Y = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(X, Y, player.transform.position.z + 12);
        if (NextPoint.z <= InistiateWall.transform.position.z)
        {
            CreatePlatform();
        }
    }

    void CreatePlatform()
    {
        Instantiate(Platform, NextPoint, Quaternion.identity);
        LastPoint = NextPoint;
        float X = LastPoint.x == LeftPoint.transform.position.x ?
            RightPoint.transform.position.x : LeftPoint.transform.position.x;
        NextPoint = new Vector3(X, LastPoint.y, LastPoint.z + Distance);
        Distance = Distance + incDistance > MaxDistance ? MaxDistance : Distance + incDistance;
    }
}
