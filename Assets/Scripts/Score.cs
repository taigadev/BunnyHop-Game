﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// всё работает
public class Score : MonoBehaviour
{
    [HideInInspector]
    public int _Score;

    Text Label;

    void Start()
    {
        Label = gameObject.GetComponent<Text>();
        _Score = 0;
    }
    
    void FixedUpdate()
    {
        Label.text = "Score: " + _Score;
    }

    public void AddPoint(int n)
    {
        _Score+=n;
    }
}
