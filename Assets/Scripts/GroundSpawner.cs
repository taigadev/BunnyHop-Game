﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//всё работает
public class GroundSpawner : MonoBehaviour
{

    public GameObject InistiateWall;
    public GameObject GroundBlock;
    public GameObject MidPoint;
    
    private float Y { get; set; }
    private float NextZ { get; set; }
    // X - определяется left/mid/right
    // Y - const
    // Z - координата, по которой движ 

    // расстояние между блоками 
    public const float distanceX = 8.81f;
    public const float distanceZ = 11.4f;


    void Start()
    {
        NextZ = MidPoint.transform.position.z;
        Y = MidPoint.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (NextZ <= InistiateWall.transform.position.z)
        {
            GroundSpawn();
        }
    }

    void GroundSpawn()
    {
        Instantiate(GroundBlock, new Vector3(MidPoint.transform.position.x - distanceX*3, Y, NextZ), Quaternion.identity);
        Instantiate(GroundBlock, new Vector3(MidPoint.transform.position.x - distanceX*2, Y, NextZ), Quaternion.identity);
        Instantiate(GroundBlock, new Vector3(MidPoint.transform.position.x - distanceX, Y, NextZ), Quaternion.identity);
        Instantiate(GroundBlock, new Vector3(MidPoint.transform.position.x, Y, NextZ), Quaternion.identity);
        Instantiate(GroundBlock, new Vector3(MidPoint.transform.position.x + distanceX, Y, NextZ), Quaternion.identity);
        Instantiate(GroundBlock, new Vector3(MidPoint.transform.position.x + distanceX*2, Y, NextZ), Quaternion.identity);
        Instantiate(GroundBlock, new Vector3(MidPoint.transform.position.x + distanceX*3, Y, NextZ), Quaternion.identity);
        NextZ += distanceZ;
    }
}
