﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// всё работает
public class Removers : MonoBehaviour
{
    private GameObject player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    private void Update()
    {
        transform.localRotation = new Quaternion(-player.transform.rotation.x, -player.transform.rotation.y, 
            -player.transform.rotation.z, player.transform.rotation.w);
        
    }
    private void OnTriggerStay(Collider other)
    {
        Destroy(other.gameObject);
    }
}
