﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// повешать на блок с триггером
public class ScoreHelper : MonoBehaviour {

    public Score Score;
    private bool Used;

	// Use this for initialization
	void Start () {
        Score = GameObject.Find("ScoreLabel").GetComponent<Score>();
        Used = false;
	}
	
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !Used)
        {
            Used = true;
            Score.AddPoint(1);
        }
    }
}
